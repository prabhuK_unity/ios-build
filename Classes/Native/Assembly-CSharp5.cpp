﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// category
struct category_t528F8469C667C5BC8C897C316508387D4DB13D53;
// content
struct content_t7D59A44F5C3C6026E45AE1D728757D52934B772F;
// entries
struct entries_t0756B00630D0C28840DE16F0875228C0DE04040F;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// category
struct  category_t528F8469C667C5BC8C897C316508387D4DB13D53  : public RuntimeObject
{
public:
	// System.String category::<Category>k__BackingField
	String_t* ___U3CCategoryU3Ek__BackingField_0;
	// System.String category::<Material>k__BackingField
	String_t* ___U3CMaterialU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCategoryU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(category_t528F8469C667C5BC8C897C316508387D4DB13D53, ___U3CCategoryU3Ek__BackingField_0)); }
	inline String_t* get_U3CCategoryU3Ek__BackingField_0() const { return ___U3CCategoryU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCategoryU3Ek__BackingField_0() { return &___U3CCategoryU3Ek__BackingField_0; }
	inline void set_U3CCategoryU3Ek__BackingField_0(String_t* value)
	{
		___U3CCategoryU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCategoryU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMaterialU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(category_t528F8469C667C5BC8C897C316508387D4DB13D53, ___U3CMaterialU3Ek__BackingField_1)); }
	inline String_t* get_U3CMaterialU3Ek__BackingField_1() const { return ___U3CMaterialU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMaterialU3Ek__BackingField_1() { return &___U3CMaterialU3Ek__BackingField_1; }
	inline void set_U3CMaterialU3Ek__BackingField_1(String_t* value)
	{
		___U3CMaterialU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMaterialU3Ek__BackingField_1), (void*)value);
	}
};


// content
struct  content_t7D59A44F5C3C6026E45AE1D728757D52934B772F  : public RuntimeObject
{
public:
	// System.String content::<CategoryName>k__BackingField
	String_t* ___U3CCategoryNameU3Ek__BackingField_0;
	// System.Boolean content::<ISSetValue>k__BackingField
	bool ___U3CISSetValueU3Ek__BackingField_1;
	// System.String content::<CategoryValue>k__BackingField
	String_t* ___U3CCategoryValueU3Ek__BackingField_2;
	// System.String content::<SlideValue>k__BackingField
	String_t* ___U3CSlideValueU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCategoryNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(content_t7D59A44F5C3C6026E45AE1D728757D52934B772F, ___U3CCategoryNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CCategoryNameU3Ek__BackingField_0() const { return ___U3CCategoryNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCategoryNameU3Ek__BackingField_0() { return &___U3CCategoryNameU3Ek__BackingField_0; }
	inline void set_U3CCategoryNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CCategoryNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCategoryNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CISSetValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(content_t7D59A44F5C3C6026E45AE1D728757D52934B772F, ___U3CISSetValueU3Ek__BackingField_1)); }
	inline bool get_U3CISSetValueU3Ek__BackingField_1() const { return ___U3CISSetValueU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CISSetValueU3Ek__BackingField_1() { return &___U3CISSetValueU3Ek__BackingField_1; }
	inline void set_U3CISSetValueU3Ek__BackingField_1(bool value)
	{
		___U3CISSetValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCategoryValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(content_t7D59A44F5C3C6026E45AE1D728757D52934B772F, ___U3CCategoryValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CCategoryValueU3Ek__BackingField_2() const { return ___U3CCategoryValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCategoryValueU3Ek__BackingField_2() { return &___U3CCategoryValueU3Ek__BackingField_2; }
	inline void set_U3CCategoryValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CCategoryValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCategoryValueU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSlideValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(content_t7D59A44F5C3C6026E45AE1D728757D52934B772F, ___U3CSlideValueU3Ek__BackingField_3)); }
	inline String_t* get_U3CSlideValueU3Ek__BackingField_3() const { return ___U3CSlideValueU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CSlideValueU3Ek__BackingField_3() { return &___U3CSlideValueU3Ek__BackingField_3; }
	inline void set_U3CSlideValueU3Ek__BackingField_3(String_t* value)
	{
		___U3CSlideValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSlideValueU3Ek__BackingField_3), (void*)value);
	}
};


// entries
struct  entries_t0756B00630D0C28840DE16F0875228C0DE04040F  : public RuntimeObject
{
public:
	// System.String entries::id
	String_t* ___id_0;
	// System.String entries::path
	String_t* ___path_1;
	// System.String entries::name
	String_t* ___name_2;
	// System.String entries::path_lower
	String_t* ___path_lower_3;
	// System.String entries::path_display
	String_t* ___path_display_4;
	// System.String entries::tag
	String_t* ___tag_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_0), (void*)value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_path_lower_3() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___path_lower_3)); }
	inline String_t* get_path_lower_3() const { return ___path_lower_3; }
	inline String_t** get_address_of_path_lower_3() { return &___path_lower_3; }
	inline void set_path_lower_3(String_t* value)
	{
		___path_lower_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_lower_3), (void*)value);
	}

	inline static int32_t get_offset_of_path_display_4() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___path_display_4)); }
	inline String_t* get_path_display_4() const { return ___path_display_4; }
	inline String_t** get_address_of_path_display_4() { return &___path_display_4; }
	inline void set_path_display_4(String_t* value)
	{
		___path_display_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_display_4), (void*)value);
	}

	inline static int32_t get_offset_of_tag_5() { return static_cast<int32_t>(offsetof(entries_t0756B00630D0C28840DE16F0875228C0DE04040F, ___tag_5)); }
	inline String_t* get_tag_5() const { return ___tag_5; }
	inline String_t** get_address_of_tag_5() { return &___tag_5; }
	inline void set_tag_5(String_t* value)
	{
		___tag_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tag_5), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String category::get_Category()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* category_get_Category_mC1928132A45D3CEF7F6A5E3772F8018E18243324 (category_t528F8469C667C5BC8C897C316508387D4DB13D53 * __this, const RuntimeMethod* method)
{
	{
		// public string Category { get; set; }
		String_t* L_0 = __this->get_U3CCategoryU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void category::set_Category(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void category_set_Category_m773C6F40C0B3CD633F094DB8A1F5805B475851B6 (category_t528F8469C667C5BC8C897C316508387D4DB13D53 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Category { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CCategoryU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String category::get_Material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* category_get_Material_m7F1A94FBFAE0846DE8C196EF8751692EDC24AE3D (category_t528F8469C667C5BC8C897C316508387D4DB13D53 * __this, const RuntimeMethod* method)
{
	{
		// public string Material { get; set; }
		String_t* L_0 = __this->get_U3CMaterialU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void category::set_Material(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void category_set_Material_m5BF1153D08D9EAEBE3DDEBFFD4119B40E04334D4 (category_t528F8469C667C5BC8C897C316508387D4DB13D53 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string Material { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CMaterialU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void category::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void category__ctor_m9E7C9B111A55FA6D4E930FA2C12D07D44E9FA7BA (category_t528F8469C667C5BC8C897C316508387D4DB13D53 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String content::get_CategoryName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* content_get_CategoryName_m5551A5005025050F71B66741E3BCB4F93953F738 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, const RuntimeMethod* method)
{
	{
		// public string CategoryName { get; set; }
		String_t* L_0 = __this->get_U3CCategoryNameU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void content::set_CategoryName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void content_set_CategoryName_mF4D575781CC711B42C600488A21F6B1D52755B9A (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string CategoryName { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CCategoryNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean content::get_ISSetValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool content_get_ISSetValue_mBAFB9EE2CA7F9E65BA6297353E7B89BD0FC23031 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, const RuntimeMethod* method)
{
	{
		// public bool ISSetValue { get; set; }
		bool L_0 = __this->get_U3CISSetValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void content::set_ISSetValue(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void content_set_ISSetValue_m03E51D39426321AE07F1AC3945DA3C55191E8F6F (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool ISSetValue { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CISSetValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String content::get_CategoryValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* content_get_CategoryValue_mC3BBD7DE11B954BBD006A96F4815937D6C940971 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, const RuntimeMethod* method)
{
	{
		// public string CategoryValue { get; set; }
		String_t* L_0 = __this->get_U3CCategoryValueU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void content::set_CategoryValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void content_set_CategoryValue_m14B3A46E67CC0E405A9EDC7D8949CE27629F6CBA (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string CategoryValue { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CCategoryValueU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.String content::get_SlideValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* content_get_SlideValue_mD8C14ED2663F1580A9ECAFEA21A448FBC900BBC0 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, const RuntimeMethod* method)
{
	{
		// public string SlideValue { get; set; }
		String_t* L_0 = __this->get_U3CSlideValueU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void content::set_SlideValue(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void content_set_SlideValue_m8DBEAE98DC5F30412A121E52D29F608D530725C6 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// public string SlideValue { get; set; }
		String_t* L_0 = ___value0;
		__this->set_U3CSlideValueU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void content::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void content__ctor_mCCCA623FC2441F0C89084D55DB67CA1C16E73417 (content_t7D59A44F5C3C6026E45AE1D728757D52934B772F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void entries::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void entries__ctor_mB56E141F3276D5D1091ED4900C7348E54E662BC8 (entries_t0756B00630D0C28840DE16F0875228C0DE04040F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
